# flux-dev-env-tilt

Set up a flux development environment using [tilt](https://tilt.dev/).

## Instructions

Since this setup configures notifications by default, create a slack webhook URL first.

For setting up flux slack notifications copy 
`flux/notifications/secrets.yaml.tmpl` to `flux/notifications/secrets.yaml`,
inserting valid slack webhook URLs in the secrets. This will be used to set up notifications for the `default` and `flux-system` namespaces.

Set up a local container registry and kind cluster using [ctlptl](https://github.com/tilt-dev/ctlptl/) 

```console
$ ctlptl apply -f cluster.yaml
```

Install flux and all the monitoring stack using tilt

```console
$ tilt up
```

Uninstall everything

```console
$ tilt down
```

**NOTE:** The monitoring stack is installed independent of flux because it's
meant to be used to monitor flux and need not depend on flux to be installed.

<img title="tilt-dashboard" alt="tilt dashboard" src="images/tilt-flux-dev-env.png">
