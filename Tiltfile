# Increase the k8s operation timeout to provide enough time for the
# kube-prometheus-stack helm charts to install.
update_settings(k8s_upsert_timeout_secs=180)

load('ext://helm_resource', 'helm_resource', 'helm_repo')

# Monitoring stack.
helm_repo('prometheus-community', 'https://prometheus-community.github.io/helm-charts')
helm_resource('kube-prometheus-stack',
    'prometheus-community/kube-prometheus-stack',
    namespace='monitoring',
    flags=[
        '--values', 'monitoring/kube-prom-stack-values.yaml',
        '--create-namespace',
    ],
)
helm_repo('grafana', 'https://grafana.github.io/helm-charts')
helm_resource('loki-stack',
    'grafana/loki-stack',
    namespace='monitoring',
    flags=[
        '--values', 'monitoring/loki-stack-values.yaml',
        '--create-namespace',
    ],
)

# Flux install manifest.
k8s_yaml(kustomize('./flux'))

# Flux notification configuration.
k8s_yaml(kustomize('flux/notifications'))

# Create flux monitoring configuration.
local_resource('flux-monitoring',
    cmd='kustomize build github.com/fluxcd/flux2/manifests/monitoring/monitoring-config?ref=main | kubectl apply -f -',
    resource_deps=['kube-prometheus-stack', 'flux-setup'],
    labels=['monitoring'],
)

# ---- Group the above resources ---- #

# The following groups the resources defined above and defines their
# relationship with one another. This makes sure that the resources are
# installed in proper order.

# Group monitoring services.
k8s_resource('kube-prometheus-stack',
    labels=['monitoring'],
)
k8s_resource('loki-stack',
    labels=['monitoring'],
)

# Use a local resource as the grafana service isn't managed by tilt directly.
local_resource(
    name='grafana-dashboard',
    serve_cmd='kubectl port-forward --namespace monitoring service/kube-prometheus-stack-grafana 8888:80',
    resource_deps=['kube-prometheus-stack'],
    labels=['monitoring'],
)

# Group flux resources that aren't particular to any controller.
k8s_resource(
    new_name='flux-setup',
    objects=[
        'flux-system:Namespace:default',
        'crd-controller:clusterrole',
        'crd-controller:clusterrolebinding',
        'cluster-reconciler:clusterrolebinding',
        'allow-egress:networkpolicy',
        'allow-scraping:networkpolicy',
        'allow-webhooks:networkpolicy',
        'buckets.source.toolkit.fluxcd.io:customresourcedefinition',
        'gitrepositories.source.toolkit.fluxcd.io:customresourcedefinition',
        'helmcharts.source.toolkit.fluxcd.io:customresourcedefinition',
        'helmrepositories.source.toolkit.fluxcd.io:customresourcedefinition',
        'helmreleases.helm.toolkit.fluxcd.io:customresourcedefinition',
        'kustomizations.kustomize.toolkit.fluxcd.io:customresourcedefinition',
        'imageupdateautomations.image.toolkit.fluxcd.io:customresourcedefinition',
        'imagepolicies.image.toolkit.fluxcd.io:customresourcedefinition',
        'imagerepositories.image.toolkit.fluxcd.io:customresourcedefinition',
        'alerts.notification.toolkit.fluxcd.io:customresourcedefinition',
        'providers.notification.toolkit.fluxcd.io:customresourcedefinition',
        'receivers.notification.toolkit.fluxcd.io:customresourcedefinition',
    ],
    labels=['flux'],
)

# Group flux services.
k8s_resource('helm-controller',
    objects=[
        'helm-controller:serviceaccount',
    ],
    resource_deps=['flux-setup'],
    labels=['flux'],
)
k8s_resource('kustomize-controller',
    objects=[
        'kustomize-controller:serviceaccount',
    ],
    resource_deps=['flux-setup'],
    labels=['flux'],
)
k8s_resource('image-automation-controller',
    objects=[
        'image-automation-controller:serviceaccount',
    ],
    resource_deps=['flux-setup'],
    labels=['flux'],
)
k8s_resource('image-reflector-controller',
    objects=[
        'image-reflector-controller:serviceaccount',
    ],
    resource_deps=['flux-setup'],
    labels=['flux'],
)
k8s_resource('notification-controller',
    objects=[
        'notification-controller:serviceaccount',
    ],
    resource_deps=['flux-setup'],
    labels=['flux'],
)
k8s_resource('source-controller',
    objects=[
        'source-controller:serviceaccount',
    ],
    resource_deps=['flux-setup'],
    labels=['flux'],
)
k8s_resource(
    new_name='namespace-notifications',
    objects=[
        'slack:provider:default',
        'slack:provider:flux-system',
        'default-alerts:alert',
        'flux-system-alerts:alert',
        'slack-url:secret:default',
        'slack-url:secret:flux-system',
    ],
    resource_deps=['notification-controller'],
    labels=['flux-objects']
)
